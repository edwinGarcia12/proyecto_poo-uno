<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class producto extends Model
{
    protected $fillable = [
        "nombre",
        "tipo",
        "estado",
        "precio"
    ];
}
