<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
    Protected $fillable = [
        "Nombre",
        "Apellido",
        "Cédula",
        "Dirección",
        "Teléfono",
        "Fecha de nacimiento",
        "Email"
    ];
}
