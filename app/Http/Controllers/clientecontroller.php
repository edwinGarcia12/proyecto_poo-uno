<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cliente;

class clientecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cliente(Request $request)
    {
        //dd('cliente');
        $cliente = clientes::all();
        //dd($cliente);
        return view('cliente.cliente')->with('cliente',$cliente);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crearinformacion(Request $request)
    {
        $cliente = clientes::all();
        return view('cliente.crear')->with('cliente',$Cliente);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardarinformacion(Request $request)
    {
        $this->validate($request, [
            'Nombre'=>'required',
            'Apellidos'=>'required',
            'Cédula'=>'required',
            'Dirección'=>'required',
            'Teléfono'=>'required',
            'Fecha_nacimiento'=>'required',
            'Email'=>'required',
        ]);

        $cliente = new Cliente;
        $cliente -> Nombre = $request->Nombre;
        $cliente -> Apellidos = $request->Apellidos;
        $cliente -> Cédula = $request->Cédula;
        $cliente -> Dirección = $request->Dirección;
        $cliente -> Teléfono = $request->Teléfono;
        $cliente -> Fecha_nacimiento = $request->Fecha_nacimiento;
        $cliente -> Email = $request->Email;
        $cliente -> save();
        return redirect()->route('registro.cliente');
    }
    


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
