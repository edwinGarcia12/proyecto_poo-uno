<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['auth']], function() {

    Route::get('/informacion/cliente', ['as' => 'registro.cliente', 'uses' => 'clienteController@Cliente']);
    Route::get('/crear/cliente', ['as' => 'crear.cliente', 'uses' => 'clienteController@crearinformacion']);
    Route::post('/guardar/cliente', ['as' => 'guardar.cliente', 'uses' => 'clienteController@guardarinformacion']);

});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
